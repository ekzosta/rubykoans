# EXTRA CREDIT:
#
# Create a program that will play the Greed Game.
# Rules for the game are in GREED_RULES.TXT.
#
# You already have a DiceSet class and score function you can use.
# Write a player class and a Game class to complete the project.	This
# is a free form assignment, so approach it however you desire.

class DiceGame
	COUNT_DICES = 5
	MAX_POINTS = 3000

	class DicePlayer
		attr_accessor :name
		attr_accessor :score

		def initialize
			@name = nil
			@score = 0
		end
	end

	class DiceSet
		attr_reader :sets
		attr_reader :values
		attr_reader :count_sets

		def initialize(value)
			@sets = []
			@count_sets = 0
			roll(value)
		end

		def roll(value)
			@values = []

			while value > 0 do
				@values.push(1+rand(6))
				value -= 1
			end

			@values = @values.sort
		end
		def look_up_sets
			if @values.size >= 1
				(0..@values.size - 1).each do |value1|
					temp_array = [@values[value1]]
					temp_score = complete_score(temp_array)
					if temp_score > 0
						@sets.push([temp_array, temp_score])
					end
				end
			end

			if @values.size >= 2
				(0..@values.size - 1).each do |value1|
					(value1+1..@values.size - 1).each do |value2|
						temp_array = [@values[value1], @values[value2]]
						temp_score = complete_score(temp_array)
						if temp_score > 0
							@sets.push([temp_array, temp_score])
						end
					end
				end
			end

			if @values.size >= 3
				(0..@values.size - 1).each do |value1|
					(value1+1..@values.size - 1).each do |value2|
						(value2+1..@values.size - 1).each do |value3|
							temp_array = [@values[value1], @values[value2], @values[value3]]
							temp_score = complete_score(temp_array)
							if temp_score > 0
								@sets.push([temp_array, temp_score])
							end	
						end
					end
				end
			end

			if @values.size >= 4
				(0..@values.size - 1).each do |value1|
					(value1+1..@values.size - 1).each do |value2|
						(value2+1..@values.size - 1).each do |value3|
							(value3+1..@values.size - 1).each do |value4|
								temp_array = [@values[value1], @values[value2], @values[value3], @values[value4]]
								temp_score = complete_score(temp_array)
								if temp_score > 0
									@sets.push([temp_array, temp_score])
								end
							end
						end
					end
				end
			end

			if @values.size >= 5
				(0..@values.size - 1).each do |value1|
					(value1+1..@values.size - 1).each do |value2|
						(value2+1..@values.size - 1).each do |value3|
							(value3+1..@values.size - 1).each do |value4|
								(value4+1..@values.size - 1).each do |value5|
									temp_array = [@values[value1], @values[value2], @values[value3], @values[value4], @values[value5]]
									temp_score = complete_score(temp_array)
									if temp_score > 0
										@sets.push([temp_array, temp_score])
									end
								end
							end
						end
					end
				end
			end

			if @values.size >= 6
				(0..@values.size - 1).each do |value1|
					(value1+1..@values.size - 1).each do |value2|
						(value2+1..@values.size - 1).each do |value3|
							(value3+1..@values.size - 1).each do |value4|
								(value4+1..@values.size - 1).each do |value5|
									(value5+1..@values.size - 1).each do |value6|
										temp_array = [@values[value1], @values[value2], @values[value3], @values[value4], @values[value5], @values[value6]]
										temp_score = complete_score(temp_array)
										if temp_score > 0
											@sets.push([temp_array, temp_score])
										end
									end
								end
							end
						end
					end
				end
			end
			@sets.uniq! {|value| [value.first]}
			@sets = @sets.sort_by{|x,y| y}
			@count_sets = @sets.size
		end
		def complete_score(dice)
			score = 0
			sequence = 1
			dice = dice.sort
			prev = 0

			temp = []
			dice.each do |element|

				if element == prev
					sequence += 1
				else
					sequence = 1
				end

				if element == 5 && sequence != 3
					score += 50
					temp.push(element)
				end

				if element == 1 && sequence != 3
					score += 100
					temp.push(element)
				end

				if element == 1 && sequence == 3
					score += 800
					temp.push(element)
					sequence = 1
				end

				if element !=1 && sequence == 3

					if element == 5
						score -= 100
					else
						temp.push(element)
						temp.push(element)
					end

					score += element * 100
					temp.push(element)
					sequence = 1
				end
				prev = element
			end

			if temp.size  != dice.size 
				score = 0
			end

			score
		end
	end

	def initialize
		puts `clear`
		welcome_to_game
	end

	def welcome_to_game
		count_players = 0
		@players = []

		run = true
		while run do
			puts "Welcome to Greed!"
			puts ""
			puts "1) New game"
			puts "9) Exit"
			print "   >"

			selected = gets.chomp.to_i

			if selected == 1
				new_game
				return start_game
				next
			end
			if selected == 9
				puts ""
				puts "Good luck!"
				puts ""
				run = false
				next
			end
			puts `clear`
		end
	end
	def new_game
		puts `clear`
		print "Enter count of players: "

		@count_players = gets.chomp.to_i

		puts ""
		(1..@count_players).each do |player|
			print "Enter Player#{player} name: "

			temp = []
			temp.push(DicePlayer.new)
			temp.push("Active")
			temp[0].name = gets.chomp
			@players.push(temp)
		end
	end
	def start_game
		puts `clear`

		leader_exist = -1
		one_more_step = false
		last_round = false

		player = 0
		while player < @count_players do
			if last_round
				puts "Last round!"
				puts ""
			end
			(0..@count_players-1).each do |table_player|
				if table_player == player 
					print " >> "
				end
				print @players[table_player][0].name + ": \t" + @players[table_player][0].score.to_s
				if @players[table_player][0].score >= MAX_POINTS 
					print " - Leader"
				end
				puts ""
			end

			if @players[player][1] == "Active"
				puts ""
				puts "#{@players[player][0].name}, your step!"
				@players[player][0].score += step

				# if @players[player][0].score >= MAX_POINTS && leader_exist < 0
				# 	@players[player][1] = "Leader"
				# 	leader_exist = player
				# 	one_more_step = true
				# 	puts `clear`
				# 	next
				# end
				if @players[player][0].score >= MAX_POINTS
					@players[player][1] = "Leader"
					if leader_exist < 0
						leader_exist = player
						one_more_step = true
					end
					puts `clear`
					next
				end
			else
				puts `clear`
			end

			if  player == @count_players-1 
				if leader_exist >= 0
					if one_more_step 
						one_more_step = false
						last_round = true
						player = 0
						next
					else
						@players = @players.sort_by{|x,y| [-x.score]}
						@players[0][1] = "Winner"
						return end_game
					end
				else
					player = 0
					next
				end
			end
			player += 1
		end
	end
	def end_game
		puts `clear`
		puts @players[0][0].name + ", winner!!!"
		puts ""
		puts "Scoreboard:"
		(0..@count_players-1).each do |table_player|
			puts @players[table_player][0].name + ": \t" + @players[table_player][0].score.to_s
		end

		puts ""
		puts ""
		return welcome_to_game
	end

	def step

		dice_sets = []
		dices = COUNT_DICES
		drops = 0
		score = 0
		while dices > 0 do
			puts ""
			puts "Press Enter to drop."
			gets.chomp

			puts `clear`
			
			dice_sets.push(DiceSet.new(dices))
			puts "Your score: #{score}"
			puts ""
			puts "You drop dice-set:"
			puts dice_sets[drops].values.inspect
			puts ""

			dice_sets[drops].look_up_sets
			if dice_sets[drops].count_sets > 0
				selected = 0

				while selected == 0 
					puts "Select dice's:"

					set_num = 1
					dice_sets[drops].sets.each do |set|
						puts set_num.to_s + ") " + set[0].inspect + " = " + set[1].to_s

						set_num += 1
					end

					puts ""
					print "   >"
					selected = gets.chomp.to_i

					if selected <= 0 || selected >= set_num
						selected = 0
					end
				end

				selected -= 1

				dices -= dice_sets[drops].sets[selected][0].size
				score += dice_sets[drops].sets[selected][1]

				if dices == 0 
					puts "Great! You have bonus drop-set"
					dices = COUNT_DICES
				end

				if score >= 300 
					puts ""
					print "Save score? (y/N): "
					confirm = gets.chomp
					if confirm != "N"
						puts `clear`
						return score
					end
				end
			else
				puts "Greed!"
				puts ""
				return 0
			end

			drops += 1
		end
	end
end

DiceGame.new