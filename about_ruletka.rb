# EXTRA CREDIT:
#
# Create a program that will play the Greed Game.
# Rules for the game are in GREED_RULES.TXT.
#
# You already have a DiceSet class and score function you can use.
# Write a player class and a Game class to complete the project.	This
# is a free form assignment, so approach it however you desire.




class DiceGame
	START_MONEY = 2000
	START_SCORE = 0

	SCORE_TO_MONEY = 3
	MONEY_TO_SEQUENCE = 20
		# ( SCORE / SCORE_TO_MONEY) - ( SEQUENCE * MONEY_TO_SEQUENCE)
		# 
		# SCORE:
		# Three 1's => 1000 points
		# Three 6's =>  600 points
		# Three 5's =>  500 points
		# Three 4's =>  400 points
		# Three 3's =>  300 points
		# Three 2's =>  200 points
		# One   1   =>  100 points
		# One   5   =>   50 points
		# 
		# SEQUENCE - Array size (Entered by User)

	BONUS_CODE = 666


	class DicePlayer
		attr_accessor :score
		attr_accessor :money
		attr_accessor :name

		def initialize
			@name = nil
			@score = START_SCORE
			@money = START_MONEY
		end
	end

	class DiceSet
		attr_reader :values

		def roll(value)
		@values = (1..value).to_a

			while value >= 1 do
				@values[value-1] = 1 + rand(6)
				value -= 1
			end
		end

		def sort
			@values.sort
		end
	end	

	def initialize
		@stats = []
		load_game_file

		@player = DicePlayer.new
	end

	def start_game

		puts `clear`
		run = true
		while run do
			puts "Welcome to Dice Green Game!"
			puts ""
			puts "1) Create new player"
			puts "2) Load player"
			puts "9) Exit"
			print "   >"

			select = gets.chomp.to_i
			puts `clear`

			if select == 1 
				new_player
				new_game
				start_game_2
				next
			end

			if select == 2
				puts "Load player:"
				print "   >"

				load_game(gets.chomp)
				
				puts `clear`
				if @player.name == nil
					puts "Can't find saved game"
					puts ""
				else
					start_game_2
				end

				next
			end

			if select == 9 
				run = false
				next
			end
		end

		save_game_file
	end

	def new_player
		puts "Enter Your name:"
		@player.name = gets.chomp
		puts `clear`

		row = 0
		while @stats.size > row
			if @stats[row][0] == @player.name
				puts "Player already exist!"
				puts ""
				@player.name = nil
				row = @stats.size
				next
			end
			row += 1
		end

		if @player.name == nil
			new_player
		end
	end

	def new_game
		@player.score = START_SCORE
		@player.money = START_MONEY
	end

	def start_game_2
		run = true
		puts `clear`
		while run do
			puts ""
			puts "Welcome, #{@player.name}! Good luck!"
			puts "Your balance: #{@player.money}$"
			puts ""
			puts "1)Show score"
			puts "2)Enter bonus-code" 
			puts "3)Roll"
			puts "6)New game"
			puts "9)Main menu"
			print "   >"

			select = gets.chomp.to_i
			puts `clear`

			if select == 1 
				puts "Your score: #{@player.score}"
				next
			end

			if select == 2
				puts "Enter bonus-code"
				print "   >"

				bonus_code = gets.chomp.to_i

				puts `clear`

				if bonus_code == BONUS_CODE
					@player.money += START_MONEY
				end
				next
			end

			if select == 3
				puts "Enter numbers of sequence between 1 - 20. 1 number = 50$"
				print "   >"
				number = gets.chomp.to_i
				puts `clear`

				if number > 0 && number <= 20
					roll(number)
				else
					puts "You must enter number between 1-20 !!!"
				end
				next
			end

			if select == 6
				puts "Are you seriously want start new game? (Y/n)"
				print "   >"

				confirm = gets.chomp

				if confirm == "Y"
					new_game
				end

				puts `clear`


			end

			if select == 9 
				save_game
				run = false
				next
			end
		end
	end

	def roll(sequence)

		if @player.money >= sequence * MONEY_TO_SEQUENCE
			dice = DiceSet.new
			dice.roll(sequence)
			score = complete_score(dice)
			@player.score += score
			@player.money -= sequence * MONEY_TO_SEQUENCE

			if (score / SCORE_TO_MONEY) - (sequence * MONEY_TO_SEQUENCE) > 0
				@player.money += score/SCORE_TO_MONEY
				puts "You are winner! (+ #{score / SCORE_TO_MONEY - sequence * MONEY_TO_SEQUENCE}$)"
			else
				puts "You are loser! (- #{sequence * MONEY_TO_SEQUENCE}$)"
			end
		else
			puts "Sorry, but you haven't money.\n Get back with money!"
		end
	end

	def complete_score(dice)
		score = 0
		sequence = 0
		dice = dice.sort
		prev = dice[0]

		dice.each do |element|
			if element == prev
				sequence += 1
				else
				sequence = 0
			end

			if element == 5 && sequence != 3
				score += 50

				prev = element
			end

			if element == 1 && sequence != 3
				score += 100

				prev = element
			end

			 if element == 1 && sequence == 3
				score += 800
				sequence = 0
				next
			end

			if element !=1 && sequence == 3
				
				if element == 5
				score -= 100
				end

				score += element * 100
				sequence = 0
				next
			end
		end
		score
	end

	def load_game_file
		open("DGG.data", "r+") do |file|
			count = 1
			temp = []
			while line = file.gets
				temp.push(line.strip)

				if count == 3
					@stats.push(temp)
					temp = []
					count =0
				end

				count += 1

			end
		end
	end

	def save_game_file
		open("DGG.data", "r+") do |file|
			file.puts(@stats)
		end
	end

	def load_game(player_name)
		row = 0
		while @stats.size > row
			if @stats[row][0] == player_name
				@player.name = @stats[row][0]
				@player.score = @stats[row][1].to_i
				@player.money = @stats[row][2].to_i
				row = @stats.size
				next
			end
			row += 1
		end		
	end

	def save_game
		row = 0
		exist = false
		while @stats.size > row
			if @stats[row][0] == @player.name
				@stats[row][0] = @player.name
				@stats[row][1] = @player.score
				@stats[row][2] = @player.money
				row = @stats.size
				exist = true
				next
			end
			row += 1
		end



		if exist == false
			temp = []
			temp.push(@player.name)
			temp.push(@player.score)
			temp.push(@player.money)

			@stats.push(temp)
		end
	end

end


DiceGame.new.start_game